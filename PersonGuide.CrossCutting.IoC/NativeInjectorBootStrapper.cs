﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PersonGuide.Application;
using PersonGuide.Domain.Interfaces.Perosn;
using PersonGuide.Domain.Interfaces.Person;
using PersonGuide.Domain.Interfaces.RelatedPerson;
using PersonGuide.Persistence.Context;
using PersonGuide.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.CrossCutting.IoC
{
    public class NativeInjectorBootStrapper
    {
        public static void RegisterServices(IServiceCollection services, IConfiguration configuration)
        {
            // Persistence
            services.AddDbContext<PersonGuideContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("PersonGuide")));

            services.AddScoped<IPersonWriteOnlyRepository, PersonRepository>();
            services.AddScoped<IPersonReadOnlyRepository, PersonRepository>();
            services.AddScoped<IRelatedPersonWriteOnlyRepository, RelatedPersonRepository>();

            //Application
            DependencyInjection.AddApplication(services);

            
        }
    }
}
