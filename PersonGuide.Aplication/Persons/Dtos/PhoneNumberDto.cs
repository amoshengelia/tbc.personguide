﻿using PersonGuide.Application.Persons.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Application.Persons.Dtos
{
    public class PhoneNumberDto
    {
        public int Id { get; set; }
        public string PhoneNumber { get; set; }

        public PhoneTypeEnum PhoneType { get; set; }
    }
}
