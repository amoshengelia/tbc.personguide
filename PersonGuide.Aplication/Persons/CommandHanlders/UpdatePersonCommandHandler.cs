﻿using MediatR;
using PersonGuide.Application.Persons.Commands;
using PersonGuide.Domain.Entities;
using PersonGuide.Domain.Interfaces.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PersonGuide.Application.Persons.CommandHanlders
{
    public class UpdatePersonCommandHandler:IRequestHandler<UpdatePersonCommand,bool>
    {
        private readonly IPersonWriteOnlyRepository _writeRepository;
        public UpdatePersonCommandHandler(IPersonWriteOnlyRepository writeRepository)
        {
            _writeRepository = writeRepository;
        }
        public async Task<bool> Handle(UpdatePersonCommand request, CancellationToken cancellationToken)
        {
            var person = new Person(request.Id,request.FirstName, request.LastName, request.Gender, request.PersonalNumber, request.BirthDate
               , request.PhoneNumbers.Select(a => new PhoneNumber(a.PhoneNumber, (int)a.PhoneType,a.Id)).ToList(), request.CityId);
            await _writeRepository.UpdatePerson(person);
           return await _writeRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}
