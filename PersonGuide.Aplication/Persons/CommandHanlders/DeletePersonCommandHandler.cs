﻿using MediatR;
using PersonGuide.Application.Persons.Commands;
using PersonGuide.Domain.Interfaces.Person;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PersonGuide.Application.Persons.CommandHanlders
{
    public class DeletePersonCommandHandler : IRequestHandler<DeletePersonCommand, bool>
    {
        private readonly IPersonWriteOnlyRepository _writeRepository;
        public DeletePersonCommandHandler(IPersonWriteOnlyRepository writeRepostiory)
        {
            _writeRepository = writeRepostiory;
        }
        public async Task<bool> Handle(DeletePersonCommand request, CancellationToken cancellationToken)
        {
            await _writeRepository.DeletePerson(request.Id);
            return await _writeRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}
