﻿using MediatR;
using PersonGuide.Application.Persons.Commands;
using PersonGuide.Domain.Entities;
using PersonGuide.Domain.Interfaces.Person;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PersonGuide.Application.Persons.CommandHanlders
{
    public class UploadImageCommandHandler : IRequestHandler<UploadImageCommand, bool>
    {
        private readonly IPersonWriteOnlyRepository _writeRepository;
        public UploadImageCommandHandler(IPersonWriteOnlyRepository writeRepository)
        {
            _writeRepository = writeRepository;
        }
        public async Task<bool> Handle(UploadImageCommand request, CancellationToken cancellationToken)
        {
            var person = new Person(request.PersonId, request.ImagePath);
            await _writeRepository.ImageUpload(person);
            return await _writeRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}
