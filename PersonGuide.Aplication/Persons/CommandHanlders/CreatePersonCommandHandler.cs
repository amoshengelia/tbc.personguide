﻿using MediatR;
using PersonGuide.Application.Commands;
using PersonGuide.Application.Handlers;
using PersonGuide.Application.Persons.Commands;
using PersonGuide.Domain.Entities;
using PersonGuide.Domain.Interfaces.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PersonGuide.Application.Persons.CommandHanlders
{
    public class CreatePersonCommandHandler : IRequestHandler<CreatePersonCommand,bool>
    {
        private readonly IPersonWriteOnlyRepository _writeRepository;
        public CreatePersonCommandHandler(IPersonWriteOnlyRepository writeRepostiory)
        {
            _writeRepository = writeRepostiory;
        }
        public async Task<bool> Handle(CreatePersonCommand request, CancellationToken cancellationToken)
        {
            var person = new Person(request.FirstName, request.LastName, request.Gender, request.PersonalNumber, request.BirthDate
                , request.PhoneNumbers.Select(a => new PhoneNumber(a.PhoneNumber, (int)a.PhoneType)).ToList(), request.CityId);
           
            await _writeRepository.CreatePerson(person);

           return await _writeRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}
