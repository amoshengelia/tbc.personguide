﻿using Microsoft.Extensions.Localization;
using PersonGuide.Application.Persons.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Application.Persons.Validations
{
    public class UpdatePersonCommandValidation : PersonValidation<UpdatePersonCommand>
    {
        public UpdatePersonCommandValidation(IStringLocalizer<ValidationResources> localizer)
        {
            ValidateName(localizer);
            ValidateSurname(localizer);
            ValidateBirthDate(localizer);
            ValidatePersonalNumber(localizer);
            ValidatePhoneNumbers(localizer);
            ValidateCity(localizer);
        }
    }
}
