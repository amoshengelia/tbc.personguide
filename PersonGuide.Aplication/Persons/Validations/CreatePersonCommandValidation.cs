﻿using Microsoft.Extensions.Localization;
using PersonGuide.Application.Persons.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Application.Persons.Validations
{
    public class CreatePersonCommandValidation: PersonValidation<CreatePersonCommand>
    {
        public CreatePersonCommandValidation(IStringLocalizer<ValidationResources> localizer)
        {
            ValidateName(localizer);
            ValidateSurname(localizer);
            ValidateBirthDate(localizer);
            ValidatePersonalNumber(localizer);
            ValidatePhoneNumbers(localizer);
            ValidateCity(localizer);
        }
    }
}
