﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using PersonGuide.Application.Persons.Commands;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace PersonGuide.Application.Persons.Validations
{
    public abstract class PersonValidation<T> : AbstractValidator<T> where T : PersonCommand
    {
        public static Regex Geo { get; set; } = new Regex(@"^[ა-ჰ]+$");
        public static Regex Latin { get; set; } = new Regex(@"^[a-zA-Z]+$");
        protected void ValidateName(IStringLocalizer<ValidationResources> localizer)
        {
            RuleFor(a => a.FirstName)
                .NotEmpty().WithMessage(localizer["FirstNameNotEmpty"])
                .Must(CombinedName).WithMessage(localizer["FirstnameSymbols"])
                .Length(2, 50).WithMessage(localizer["FirstNameLength"]);
        }
        protected void ValidateSurname(IStringLocalizer<ValidationResources> localizer)
        {
            RuleFor(a => a.LastName)
               .NotEmpty().WithMessage(localizer["LastNameNotEmpty"])
               .Length(2, 50).WithMessage(localizer["LastNameLength"])
                .Must(CombinedLastName).WithMessage(localizer["LastNameSymbols"]);
        }

        protected void ValidateBirthDate(IStringLocalizer<ValidationResources> localizer)
        {
            RuleFor(a => a.BirthDate)
                .NotEmpty()
                .Must(HaveMinimumAge)
                .WithMessage(localizer["BirthDate"]);
        }

        protected void ValidatePersonalNumber(IStringLocalizer<ValidationResources> localizer)
        {
            RuleFor(a => a.PersonalNumber)
                .NotEmpty()
                .Must(a => a.Length.Equals(11))
                .WithMessage(localizer["PersonalNumberLength"]);

        }
        protected void ValidateCity(IStringLocalizer<ValidationResources> localizer)
        {
            RuleFor(a => a.CityId).NotEmpty().GreaterThan(0).LessThan(4)
                .WithMessage(localizer["City"]);
        }

        protected void ValidatePhoneNumbers(IStringLocalizer<ValidationResources> localizer)
        {

            RuleForEach(a => a.PhoneNumbers).SetValidator(new PhoneValidation(localizer));
        }
        protected static bool HaveMinimumAge(DateTime birthDate)
        {
            return birthDate <= DateTime.Now.AddYears(-18);
        }
        protected static bool CombinedName(string firstName)
        {
            if (Geo.IsMatch(firstName) || Latin.IsMatch(firstName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        protected static bool CombinedLastName(string lastName)
        {
            if (Geo.IsMatch(lastName) || Latin.IsMatch(lastName))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
