﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using PersonGuide.Application.Persons.Commands;
using PersonGuide.Application.Persons.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Application.Persons.Validations
{
    public class PhoneValidation : AbstractValidator<PhoneNumberDto> 
    {
        public PhoneValidation(IStringLocalizer<ValidationResources> localizer)
        {
            ValidatePhoneNumber(localizer);
            ValidatePhoneType(localizer);
        }
        protected void ValidatePhoneNumber(IStringLocalizer<ValidationResources> localizer)
        {
            RuleFor(a => a.PhoneNumber)
                .Length(4, 50).WithMessage(localizer["PhoneNumberLength"]);
        }
        protected void ValidatePhoneType(IStringLocalizer<ValidationResources> localizer)
        {
            RuleFor(a => a.PhoneType).IsInEnum()
                .WithMessage(localizer["PhoneType"]);
        }
    }
}
