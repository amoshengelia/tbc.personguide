﻿using PersonGuide.Application.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Application.Persons.Commands
{
    public class DeletePersonCommand:Command
    {
        public int Id { get; set; }
    }
}
