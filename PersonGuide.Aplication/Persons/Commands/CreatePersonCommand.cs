﻿using MediatR;
using PersonGuide.Application.Commands;
using PersonGuide.Application.Persons.Dtos;
using PersonGuide.Application.Persons.Enums;
using PersonGuide.Application.Persons.Validations;
using PersonGuide.Domain.Entities;
using PersonGuide.Domain.Interfaces.Person;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PersonGuide.Application.Persons.Commands
{
    public class CreatePersonCommand : PersonCommand
    {
      
    }
}
