﻿using PersonGuide.Application.Commands;
using PersonGuide.Application.Persons.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Application.Persons.Commands
{
    public abstract class PersonCommand:Command
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Gender { get; set; }
        public string PersonalNumber { get; set; }
        public DateTime BirthDate { get; set; }
        public int CityId { get; set; }
        public List<PhoneNumberDto> PhoneNumbers { get; set; }
    }
}
