﻿using Microsoft.AspNetCore.Http;
using PersonGuide.Application.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Application.Persons.Commands
{
    public class UploadImageCommand:Command
    {
        public string ImagePath { get; set; }
        public int PersonId { get; set; }
    }
}
