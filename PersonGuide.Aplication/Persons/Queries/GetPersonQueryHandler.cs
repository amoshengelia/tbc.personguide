﻿using MediatR;
using PersonGuide.Domain.Interfaces.Perosn;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PersonGuide.Application.Persons.Queries
{
    public class GetPersonQueryHandler : IRequestHandler<GetPersonQuery, GetPersonDetailModel>
    {
        private readonly IPersonReadOnlyRepository _readRepository;
        public GetPersonQueryHandler(IPersonReadOnlyRepository readRepository)
        {
            _readRepository = readRepository;
        }
        public async Task<GetPersonDetailModel> Handle(GetPersonQuery request, CancellationToken cancellationToken)
        {
            var person = await _readRepository.GetPerson(request.Id);
            return GetPersonDetailModel.Create(person);
        }
    }
}
