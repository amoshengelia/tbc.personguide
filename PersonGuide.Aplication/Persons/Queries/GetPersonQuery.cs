﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Application.Persons.Queries
{
    public class GetPersonQuery:IRequest<GetPersonDetailModel>
    {
        public int Id { get; set; }
    }
}
