﻿using PersonGuide.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace PersonGuide.Application.Persons.Queries
{
    public class GetPersonDetailModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Gender { get; set; }
        public string PersonalNumber { get; set; }
        public DateTime BirthDate { get; set; }
        public string Image { get; set; }
        public int CityId { get; set; }
        public List<PhoneNumber> PhoneNumbers { get; set; }
        public List<RelatedPerson> RelatedPersons { get; set; }
        public static Expression<Func<Person, GetPersonDetailModel>> Projection
        {
            get
            {
                return personDetail => new GetPersonDetailModel
                {
                    BirthDate = personDetail.BirthDate,
                    CityId = personDetail.CityId,
                    FirstName = personDetail.FirstName,
                    Gender = personDetail.Gender,
                    Image = personDetail.Image,
                    LastName = personDetail.LastName,
                    PersonalNumber = personDetail.PersonalNumber,
                    PhoneNumbers = personDetail.PhoneNumbers.Select(a => new PhoneNumber()
                    {
                        Number=a.Number,
                        Type=a.Type
                    }).ToList(),
                    RelatedPersons=personDetail.RelatedPersons.Select(a=> new RelatedPerson()
                    {
                         Type=a.PersonType
                    }).ToList()
                };
            }
        }
        public static GetPersonDetailModel Create(Person person)
        {
            return Projection.Compile().Invoke(person);
        }
    }
    public class PhoneNumber
    {
        public string Number { get;  set; }
        public int Type { get;  set; }
    }
    public class RelatedPerson
    {
        public int Type { get; set; }
    }
}
