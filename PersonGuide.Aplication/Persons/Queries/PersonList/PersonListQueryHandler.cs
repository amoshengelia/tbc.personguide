﻿using MediatR;
using PersonGuide.Domain.Interfaces.Perosn;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PersonGuide.Application.Persons.Queries.PersonList
{
    public class PersonListQueryHandler : IRequestHandler<PersonListQuery, PersonList>
    {
        private readonly IPersonReadOnlyRepository _readRepository;
        public PersonListQueryHandler(IPersonReadOnlyRepository readRepository)
        {
            _readRepository = readRepository;
        }
        public async Task<PersonList> Handle(PersonListQuery request, CancellationToken cancellationToken)
        {
            var personEntityList = await _readRepository.GetPersons(request.PageNumber.Value, request.PageSize,
                request.SearchQuery, request.SearchBirthdate, request.SearchCity, request.SearchGender);
            var personList = personEntityList.Select(a => new PersonLookUpDto()
            {
                BirthDate = a.BirthDate,
                CityId = a.CityId,
                FirstName = a.FirstName,
                Gender = a.Gender,
                LastName = a.LastName,
                PersonalNumber = a.PersonalNumber
            }).ToList();
            var result = new PersonList()
            {
                CurrentPage = personEntityList?.FirstOrDefault()?.Pagenation.CurrentPage ,
                Persons = personList,
                PageSize = personEntityList?.FirstOrDefault()?.Pagenation.PageSize,
                TotalCount = personList.Count,
                TotalPages = personEntityList?.FirstOrDefault()?.Pagenation.PageCount
            };
            return result;
        }
    }
}
