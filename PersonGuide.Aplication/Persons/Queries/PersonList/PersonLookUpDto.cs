﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Application.Persons.Queries.PersonList
{
    public class PersonLookUpDto
    {
        public string FirstName { get;  set; }
        public string LastName { get;  set; }
        public bool Gender { get;  set; }
        public string PersonalNumber { get;  set; }
        public DateTime BirthDate { get;  set; }
        public int CityId { get;  set; }
    }
}
