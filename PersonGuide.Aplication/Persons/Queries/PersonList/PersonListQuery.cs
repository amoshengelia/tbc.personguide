﻿using MediatR;
using PersonGuide.Common.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Application.Persons.Queries.PersonList
{
    public class PersonListQuery: IUrlQuery,IRequest<PersonList>
    {
        public int MaxsPagesize { get; set; }
        public bool IncludeCount { get; set; }
        public int? PageNumber { get; set; }
        public int PageSize { get; set; } = 10;
        public string SearchQuery { get; set; }
        public bool? SearchGender { get; set; }
        public DateTime? SearchBirthdate { get; set; }
        public int? SearchCity { get; set; }

    }
}
