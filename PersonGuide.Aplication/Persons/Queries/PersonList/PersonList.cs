﻿using PersonGuide.Common.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Application.Persons.Queries.PersonList
{
    public class PersonList: PageResult
    {
        public IList<PersonLookUpDto> Persons { get; set; }
    }
}
