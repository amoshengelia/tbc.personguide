﻿using MediatR;
using PersonGuide.Application.RelatedPerons.Enums;
using PersonGuide.Domain.Interfaces.Perosn;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PersonGuide.Application.Persons.Queries.NumberOfPeronRelatedPerson
{
    public class PersonReportListQueryHandler : IRequestHandler<PersonReportListQuery, PersonReportList>
    {
        private readonly IPersonReadOnlyRepository _readRepository;
        public PersonReportListQueryHandler(IPersonReadOnlyRepository readRepository)
        {
            _readRepository = readRepository;
        }

        public async Task<PersonReportList> Handle(PersonReportListQuery request, CancellationToken cancellationToken)
        {
            var personReport = await _readRepository.GetPersonsReport();
            var personReportList = personReport.Select(a => new PersonReportLookUpDto()
            {
                PersonId = a.Id,
                RelatedPersonType = a.RelatedPersonTypeId,
                RelatedPersonCount = a.Count
            }).ToList();
            var result = new PersonReportList
            {
                Persons = personReportList
            };
            return result;
        }
    }
}
