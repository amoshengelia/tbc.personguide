﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Application.Persons.Queries.NumberOfPeronRelatedPerson
{
    public class PersonReportList
    {
        public IList<PersonReportLookUpDto> Persons { get; set; }

    }
}
