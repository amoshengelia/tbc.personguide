﻿using PersonGuide.Application.RelatedPerons.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Application.Persons.Queries.NumberOfPeronRelatedPerson
{
    public class PersonReportLookUpDto
    {
        public int PersonId { get; set; }
        public int RelatedPersonCount { get; set; }
        public int RelatedPersonType { get; set; }
    }
}
