﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Application.Persons.Enums
{
    public enum PhoneTypeEnum
    {
        Mobile,
        Office,
        Home
    }
}
