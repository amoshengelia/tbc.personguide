﻿using FluentValidation.Results;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Application.Commands
{
    public abstract class Command : IRequest<bool>
    {
       
    }
    public abstract class Query<TResult> : IRequest<TResult>
    {

    }
}
