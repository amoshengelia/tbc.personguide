﻿using PersonGuide.Application.RelatedPerons.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Application.RelatedPerons.Validations
{
    public class CreateRelatedPersonValidation: RelatedPersonValidation<CreateRelatedPersonCommand>
    {
        public CreateRelatedPersonValidation()
        {
            ValidatePersonType();
        }
    }
}
