﻿using FluentValidation;
using PersonGuide.Application.RelatedPerons.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Application.RelatedPerons.Validations
{
    public class RelatedPersonValidation<T>: AbstractValidator<T> where T: CreateRelatedPersonCommand
    {
        protected void ValidatePersonType()
        {
            RuleFor(a => a.RelatedPersonType).IsInEnum().WithMessage("Type is Incorrect");
        }
    }
}
