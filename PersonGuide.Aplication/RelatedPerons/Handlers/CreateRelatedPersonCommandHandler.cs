﻿using MediatR;
using PersonGuide.Application.RelatedPerons.Commands;
using PersonGuide.Domain.Entities;
using PersonGuide.Domain.Interfaces.RelatedPerson;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PersonGuide.Application.RelatedPerons.Handlers
{
    public class CreateRelatedPersonCommandHandler : IRequestHandler<CreateRelatedPersonCommand, bool>
    {
        private readonly IRelatedPersonWriteOnlyRepository _writeRepository;
        public CreateRelatedPersonCommandHandler(IRelatedPersonWriteOnlyRepository writeRepository)
        {
            _writeRepository = writeRepository;
        }
        public async Task<bool> Handle(CreateRelatedPersonCommand request, CancellationToken cancellationToken)
        {
            var relatedPerson = new RelatedPerson(request.PersonId, (int)request.RelatedPersonType);
            await _writeRepository.CreateRelatedPerson(relatedPerson);
           return await _writeRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}
