﻿using MediatR;
using PersonGuide.Application.RelatedPerons.Commands;
using PersonGuide.Domain.Interfaces.RelatedPerson;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PersonGuide.Application.RelatedPerons.Handlers
{
    public class DeleteRelatedPersonCommandHandler : IRequestHandler<DeleteRelatedPersonCommand, bool>
    {
        private readonly IRelatedPersonWriteOnlyRepository _writeRepository;
        public DeleteRelatedPersonCommandHandler(IRelatedPersonWriteOnlyRepository writeRepository)
        {
            _writeRepository = writeRepository;
        }
        public async Task<bool> Handle(DeleteRelatedPersonCommand request, CancellationToken cancellationToken)
        {
            await _writeRepository.DeleteRelatedPerson(request.RelatedPersonId);
            return await _writeRepository.UnitOfWork.SaveEntitiesAsync();
        }
    }
}
