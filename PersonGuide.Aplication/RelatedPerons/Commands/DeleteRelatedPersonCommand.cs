﻿using PersonGuide.Application.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Application.RelatedPerons.Commands
{
    public class DeleteRelatedPersonCommand:Command
    {
        public int RelatedPersonId { get; set; }
    }
}
