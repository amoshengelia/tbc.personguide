﻿using PersonGuide.Application.Commands;
using PersonGuide.Application.RelatedPerons.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Application.RelatedPerons.Commands
{
    public class CreateRelatedPersonCommand:Command
    {
        public int PersonId { get; set; }
        public RelatedPersonTypeEnum RelatedPersonType { get; set; }
    }
}
