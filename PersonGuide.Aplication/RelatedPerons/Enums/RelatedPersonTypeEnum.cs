﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Application.RelatedPerons.Enums
{
    public enum RelatedPersonTypeEnum
    {
        Colleague,
        Aquainted,
        Relative,
        Other
    }
}
