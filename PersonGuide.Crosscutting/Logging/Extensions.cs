﻿using Microsoft.Extensions.Hosting;
using System;
using Serilog;
using Serilog.Events;
using Microsoft.Extensions.Configuration;

namespace PersonGuide.CrossCutting.Logging.Logging
{
    public static class Extensions
    {
        public static IHostBuilder UseLogging(this IHostBuilder webHostBuilder, string applicationName = null)
         => webHostBuilder.UseSerilog((context, loggerConfiguration) =>
         {
             var serilogOptions = context.Configuration.GetOptions<SerilogOptions>("serilog");
             if (!Enum.TryParse<LogEventLevel>(serilogOptions.Level, true, out var level))
             {
                 level = LogEventLevel.Information;
             }

             loggerConfiguration.Enrich.FromLogContext()
                 .MinimumLevel.Is(level)
                 .Enrich.WithProperty("Environment", context.HostingEnvironment.EnvironmentName);
             Configure(loggerConfiguration, level, serilogOptions);
         });
        private static void Configure(LoggerConfiguration loggerConfiguration, LogEventLevel level
           , SerilogOptions serilogOptions)
        {
           
            if (serilogOptions.ConsoleEnabled)
            {
                loggerConfiguration.WriteTo.Console();
            }
            if (serilogOptions.FileEnabled)
            {
                loggerConfiguration.WriteTo.File("Logs/log.txt");
            }
           
        }
        public static TModel GetOptions<TModel>(this IConfiguration configuration, string section) where TModel : new()
        {
            var model = new TModel();
            configuration.GetSection(section).Bind(model);
            return model;
        }
    }
}
