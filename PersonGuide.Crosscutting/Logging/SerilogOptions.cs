﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.CrossCutting.Logging.Logging
{
    public class SerilogOptions
    {
        public bool ConsoleEnabled { get; set; }
        public bool FileEnabled { get; set; }
        public string Level { get; set; }
    }
}
