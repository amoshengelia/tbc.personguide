﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PersonGuide.Application.Persons.Commands;
using PersonGuide.Application.Persons.Queries;
using PersonGuide.Application.Persons.Queries.NumberOfPeronRelatedPerson;
using PersonGuide.Application.Persons.Queries.PersonList;
using PersonGuide.Common.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PersonGuide.Api.Controllers
{
    public class PersonController : BaseController
    {
        private readonly IWebHostEnvironment _env;
        public PersonController(IWebHostEnvironment env)
        {
            _env = env;
        }
        [HttpPost("CreatePerson")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreatePerson([FromBody]CreatePersonCommand command)
        {
            await Mediator.SendCommand(command);
            return NoContent();
        }
        [HttpPost("UpdatePerson")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UpdatePerson([FromBody]UpdatePersonCommand command)
        {
            await Mediator.SendCommand(command);
            return NoContent();
        }
        [HttpPost("UploadPersonImage")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UploadPersonImage(IFormFile image, int personId)
        {
            string fileName = DateTime.Now.ToString("yyyyMMddHHmmss") + "_" +
                new Random().Next(1, 100).ToString() + $"{Path.GetExtension(image.FileName)}" + personId + ".jpg";
            var uploadsFolder = Path.Combine(_env.WebRootPath, "Images");
            var filePath = Path.Combine(uploadsFolder, fileName);
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                await image.CopyToAsync(fileStream);
            }
            await Mediator.SendCommand(new UploadImageCommand { ImagePath = filePath, PersonId = personId });
            return NoContent();
        }

        [HttpGet("GetPerson")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<GetPersonDetailModel>> GetPerson(int personId)
        {
            var person = await Mediatr.Send(new GetPersonQuery() { Id = personId });
            return Ok(person);
        }
        [HttpGet("GetAllPersons")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<PersonList>> GetAllPersons([FromQuery]UrlQuery urlQuery)
        {
            var person = await Mediatr.Send(new PersonListQuery()
            {
                PageNumber = urlQuery.PageNumber ?? 1,
                SearchQuery = urlQuery.SearchQuery,
                SearchBirthdate = urlQuery.SearchBirthdate,
                SearchCity = urlQuery.SearchCity,
                SearchGender = urlQuery.SearchGender
            });
            if (person != null)
                return Ok(person);
            else
                return NotFound(); ;
        }

        [HttpGet("GetPersonReport")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesDefaultResponseType]
        public async Task<ActionResult<PersonReportList>> GetPersonReport()
        {
            var personReport = await Mediatr.Send(new PersonReportListQuery() { });
           
            if (personReport != null)
                return Ok(personReport);
            else
                return NotFound(); ;
        }
        [HttpPost("DeletePerson")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeletePerson(int id)
        {
            await Mediator.SendCommand(new DeletePersonCommand() { Id = id });
            return NoContent();
        }
    }
}
