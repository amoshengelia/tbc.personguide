﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using PersonGuide.Application.Handlers;

namespace PersonGuide.Api.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class BaseController : ControllerBase
    {
        private IMediatorHandler _mediator;
        protected IMediatorHandler Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediatorHandler>();
        private IMediator _mediatr;
        protected IMediator Mediatr => _mediatr ??= HttpContext.RequestServices.GetService<IMediator>();
    }
}
