﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PersonGuide.Application.RelatedPerons.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonGuide.Api.Controllers
{
    public class RelatedPersonController : BaseController
    {

        [HttpPost("CreateRelatedPerson")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateRelatedPerson([FromBody]CreateRelatedPersonCommand command)
        {
            await Mediator.SendCommand(command);
            return NoContent();
        }
        [HttpPost("DeleteRelatedPerson")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeleteRelatedPerson(int id)
        {
            await Mediator.SendCommand(new DeleteRelatedPersonCommand() {  RelatedPersonId=id});
            return NoContent();
        }
    }
}
