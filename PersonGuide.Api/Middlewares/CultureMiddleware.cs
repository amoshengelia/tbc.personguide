﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace PersonGuide.Api.Middlewares
{
    public class CultureMiddleware
    {
        private readonly RequestDelegate _next;

        public CultureMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var acceptedCultures = new string[] { "ka", "en" };
            var listOfLanguages = context.Request.Headers["Accept-Language"].ToString() ?? string.Empty;
            var language = listOfLanguages.Split(',').FirstOrDefault()?.Split('-').FirstOrDefault();
            language = string.IsNullOrWhiteSpace(language) || language.Length != 2 || !acceptedCultures.Contains(language) ? "en" : language;

            try
            {
                CultureInfo.CurrentCulture = new CultureInfo(language);
                CultureInfo.CurrentUICulture = new CultureInfo(language);
            }
            catch (Exception) { }

            await _next.Invoke(context);
        }
    }

    public static class CultureExtensions
    {
        public static IApplicationBuilder UseCulture(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CultureMiddleware>();
        }
    }
}
