﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Common.Exceptions
{
    public class RelatedUserNotFoundException:Exception
    {
        public RelatedUserNotFoundException()
        { }

        public RelatedUserNotFoundException(string message)
            : base(message)
        { }
    }
}
