﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Common.Exceptions
{
    public class UserNotFoundException: Exception
    {
        public UserNotFoundException()
        { }

        public UserNotFoundException(string message)
            : base(message)
        { }
    }
}
