﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Common.Types
{
    public class PagedResult<T> :PagedResultBase where T :class
    {
        public ICollection<T> Results { get; set; }
        public PagedResult()
        {
            Results = new List<T>();
        }
    }
}
