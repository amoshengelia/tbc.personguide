﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Common.Types
{
    public class UrlQuery : IUrlQuery
    {
        public int MaxsPagesize { get; set; } = 15;
        public int? PageNumber { get; set; }
        private int _pageSize = 10;
        public string SearchQuery { get; set; }
        public bool? SearchGender { get; set; }
        public DateTime? SearchBirthdate { get; set; }
        public int? SearchCity { get; set; }
        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = (value < MaxsPagesize) ? value : MaxsPagesize;
            }
        }

    }
}
