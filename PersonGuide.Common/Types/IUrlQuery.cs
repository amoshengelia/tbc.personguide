﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Common.Types
{
    public interface IUrlQuery
    {
        int? PageNumber { get; set; }
        int PageSize { get; set; }
        string SearchQuery { get; set; }
        bool? SearchGender { get; set; }
        DateTime? SearchBirthdate { get; set; }
        int? SearchCity { get; set; }
    }
}
