﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Common.Types
{
    public class Pagenation : IPagable
    {
        public int CurrentPage { get; set; }
        public int PageCount { get; set; }
        public int PageSize { get; set; }
        public int RowCount { get; set; }
        public Pagenation()
        {

        }
        public Pagenation(int currentPage, int pageCount, int pageSize, int rowCount)
        {
            CurrentPage = currentPage;
            PageCount = pageCount;
            PageSize = pageSize;
            RowCount = rowCount;
        }
    }
}
