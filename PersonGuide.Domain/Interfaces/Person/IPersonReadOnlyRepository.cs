﻿using PersonGuide.Domain.Tools;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PersonGuide.Domain.Interfaces.Perosn
{
    public interface IPersonReadOnlyRepository:IRepository
    {
        Task<Entities.Person> GetPerson(int id);
        Task<List<Entities.Person>> GetPersons(int pageNumber, int pageSize, 
            string searchQuery,DateTime? searchBirthdate,int? searchcityId,bool? searchGender);
        Task<List<Entities.Person>> GetPersonsReport();

    }
}
