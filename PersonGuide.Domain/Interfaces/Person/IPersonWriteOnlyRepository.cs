﻿using PersonGuide.Domain.Tools;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PersonGuide.Domain.Interfaces.Person
{
    public interface IPersonWriteOnlyRepository: IRepository
    {
        Task CreatePerson(Entities.Person person);

        Task UpdatePerson(Entities.Person person);

        Task ImageUpload(Entities.Person peron);

        Task DeletePerson(int id);
    }
}
