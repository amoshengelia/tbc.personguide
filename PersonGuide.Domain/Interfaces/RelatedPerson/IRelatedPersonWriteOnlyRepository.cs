﻿using PersonGuide.Domain.Tools;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PersonGuide.Domain.Interfaces.RelatedPerson
{
    public  interface IRelatedPersonWriteOnlyRepository: IRepository
    {
        Task CreateRelatedPerson(Entities.RelatedPerson person);
        Task DeleteRelatedPerson(int id);
    }
}
