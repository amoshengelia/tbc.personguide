﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PersonGuide.Domain.Tools
{
    public interface IUnitOfWork
    {
        Task<bool> SaveEntitiesAsync();
    }
}
