﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Domain.Exceptions
{
    public class PersonDomainException:Exception
    {
        public PersonDomainException()
        { }

        public PersonDomainException(string message)
            : base(message)
        { }
        public PersonDomainException(List<string> message) : base(message.ToString())
        {

        }
        public PersonDomainException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
