﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Domain.Entities
{
    public class PhoneNumber
    {
        public int Id { get;protected set; }
        public string Number { get;protected set; }
        public int Type { get; protected set; }
        public PhoneNumber(string number,int type)
        {
            Number = number;
            Type = type;
        }
        public PhoneNumber(string number, int type,int id)
        {
            Number = number;
            Type = type;
            Id = id;
        }
    }
}
