﻿using PersonGuide.Common.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Domain.Entities
{
    public class Person
    {
        protected Person() { _phoneNumbers = new List<PhoneNumber>(); _relatedPersons = new List<RelatedPerson>(); }
        public int Id { get;protected set; }
        public string FirstName { get;protected set; }
        public string LastName { get; protected set; }
        public bool Gender { get; protected set; }
        public string PersonalNumber { get; protected set; }
        public DateTime BirthDate { get; protected set; }
        public string Image { get; protected set; }
        public int CityId { get;protected set; }
        public int Count { get;protected set; }
        public int RelatedPersonTypeId { get; set; }
        public Pagenation Pagenation { get; set; }

        private List<PhoneNumber> _phoneNumbers;
        public IEnumerable<PhoneNumber> PhoneNumbers => _phoneNumbers;

        public List<RelatedPerson> _relatedPersons;
        public IEnumerable<RelatedPerson> RelatedPersons => _relatedPersons.AsReadOnly();
        public Person(string firstName, string lastName, bool gender, string personalNumber, DateTime birthDate,
            List<PhoneNumber> numbers,int cityId)
        {
            FirstName = firstName;
            LastName = lastName;
            Gender = gender;
            PersonalNumber = personalNumber;
            BirthDate = birthDate;
            _phoneNumbers = numbers;
            CityId = cityId;
        }
        public Person(int id,string firstName, string lastName, bool gender, string personalNumber, DateTime birthDate,
            List<PhoneNumber> numbers, int cityId)
        {
            FirstName = firstName;
            LastName = lastName;
            Gender = gender;
            PersonalNumber = personalNumber;
            BirthDate = birthDate;
            _phoneNumbers = numbers;
            CityId = cityId;
            Id = id;
        }
        public Person(int personId,string image)
        {
            Id = personId;
            Image = image;
        }
        public void AddRelatedPerons(int personType)
        {
            _relatedPersons.Add(new RelatedPerson(personType));
        }
        public void AddPhone(string phoneNumber,int phoneType)
        {
            _phoneNumbers.Add(new PhoneNumber(phoneNumber, phoneType));
        }
        public static Person Load(int id, string firstName, string lastName, 
            bool gender, string personalNumber, DateTime birthDate,int cityId,string image)
        {
            Person person = new Person();
            person.FirstName = firstName;
            person.LastName = lastName;
            person.Gender = gender;
            person.PersonalNumber = personalNumber;
            person.BirthDate = birthDate;
            person.CityId = cityId;
            person.Image = image;
            return person;
        }
        public static Person LoadAll(int id, string firstName, string lastName,
            bool gender, string personalNumber, DateTime birthDate, int cityId, int currentPage,
            int pageCount, int pageSize, int rowCount)
        {
            Person person = new Person();
            person.Pagenation = new Pagenation();
            person.Pagenation.CurrentPage = currentPage;
            person.Pagenation.PageCount = pageCount;
            person.Pagenation.PageSize = pageSize;
            person.Pagenation.RowCount = rowCount;
            person.FirstName = firstName;
            person.LastName = lastName;
            person.Gender = gender;
            person.PersonalNumber = personalNumber;
            person.BirthDate = birthDate;
            person.CityId = cityId;
            return person;
        }
        public static Person LoadReport(int id,int count,int relatedPersonTypeId)
        {
            Person person = new Person();
            person.Id = id;
            person.Count = count;
            person.RelatedPersonTypeId = relatedPersonTypeId;
            return person;
        }
    }
}
