﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Domain.Entities
{
    public class RelatedPerson
    {
        protected RelatedPerson() { }
        public int Id { get;protected set; }
        public string PersonalNumber { get; protected set; }
        public int PersonId { get; set; }
        public int PersonType { get; set; }
        public RelatedPerson(int personId,int personType)
        {
            PersonId = personId;
            PersonType = personType;
        }
        public RelatedPerson(int personType)
        {
            PersonType = personType;
        }
        
    }
}
