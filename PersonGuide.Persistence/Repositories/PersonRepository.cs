﻿using Microsoft.EntityFrameworkCore;
using PersonGuide.Application.Persons.Enums;
using PersonGuide.Common.Types;
using PersonGuide.Domain.Entities;
using PersonGuide.Domain.Interfaces.Perosn;
using PersonGuide.Domain.Interfaces.Person;
using PersonGuide.Domain.Tools;
using PersonGuide.Persistence.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonGuide.Persistence.Repositories
{
    public class PersonRepository : IPersonReadOnlyRepository, IPersonWriteOnlyRepository
    {
        private readonly PersonGuideContext _context;
        public IUnitOfWork UnitOfWork
        {
            get
            {
                return _context;
            }
        }
        public PersonRepository(PersonGuideContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));

        }
        public async Task CreatePerson(Person person)
        {
            var personEntity = new Entities.Person
            {
                BirthDate = person.BirthDate,
                PersonalNumber = person.PersonalNumber,
                FirstName = person.FirstName,
                LastName = person.LastName,
                Gender = person.Gender,
                CityId = person.CityId,
                Phones = person.PhoneNumbers.Select(a => new Entities.Phone()
                {
                    PhoneNumber = a.Number,
                    PhoneType = (Entities.Enums.PhoneTypeEnum)a.Type
                }).ToList()
            };
            await _context.Persons.AddAsync(personEntity);
        }

        public async Task DeletePerson(int id)
        {
            var person = await _context.Persons.FirstOrDefaultAsync(a => a.Id.Equals(id));
            if (person != null)
            {
                _context.Remove(person);
            }
        }

        public async Task<Person> GetPerson(int id)
        {
            var personEntity = await _context.Persons.FirstOrDefaultAsync(a => a.Id.Equals(id));
            var phones = await _context.Phones.Include(a => a.Person).Where(a => a.PersonId.Equals(id)).ToListAsync();
            var relatedPerson = await _context.RelatedPersons.Include(a => a.Person).Where(a => a.PersonId.Equals(id)).ToListAsync();
            if (personEntity == null)
            {
                throw new Common.Exceptions.UserNotFoundException("User Not Found");
            }
            var person = Person.Load(personEntity.Id, personEntity.FirstName,
            personEntity.LastName, personEntity.Gender, personEntity.PersonalNumber, personEntity.BirthDate, personEntity.CityId, personEntity.Image);
            foreach (var relatedPersons in relatedPerson)
            {
                person.AddRelatedPerons((int)relatedPersons.RelatedPersonType);
            }
            foreach (var phone in phones)
            {
                person.AddPhone(phone.PhoneNumber, (int)phone.PhoneType);
            }
            return person;
        }

        public async Task<List<Person>> GetPersons(int pageNumber, int pageSize, string searchQuery,
            DateTime? searchBirthdate, int? searchcityId, bool? searchGender)
        {
            var basepersonQuery = _context.Persons.AsQueryable();
            if (!string.IsNullOrEmpty(searchQuery))
            {
                basepersonQuery = _context.Persons.Where(a => a.FirstName.ToLower().Contains(searchQuery.ToLower())
                || a.LastName.ToLower().Contains(searchQuery.ToLower()) || a.PersonalNumber.ToLower().Contains(searchQuery.ToLower())
               ).AsQueryable();
            }

            if (searchBirthdate.HasValue)
            {
                basepersonQuery = basepersonQuery.Where(a => a.BirthDate.Date.Equals(searchBirthdate.Value.Date));
            }
            else if (searchcityId.HasValue)
            {
                basepersonQuery = basepersonQuery.Where(a => a.CityId.Equals(searchcityId));

            }
            else if (searchGender.HasValue)
            {
                basepersonQuery = basepersonQuery.Where(a => a.Gender.Equals(searchGender));

            }
            var personQuery = await basepersonQuery.GetPagedResultAsync(pageNumber, pageSize);
            List<Person> personList = new List<Domain.Entities.Person>();
            foreach (var person in personQuery.Results)
            {
                var onePerson = Person.LoadAll(person.Id, person.FirstName,
                person.LastName, person.Gender, person.PersonalNumber, person.BirthDate, person.CityId,
                personQuery.CurrentPage, personQuery.PageCount, personQuery.PageSize, personQuery.RowCount);
                personList.Add(onePerson);
            }
            return personList;
        }

        public async Task ImageUpload(Person person)
        {
            var personEntity = await _context.Persons.FirstOrDefaultAsync(a => a.Id.Equals(person.Id));
            if(personEntity != null)
            {
                personEntity.Image = person.Image;
               _context.Entry(personEntity).State = EntityState.Modified;
            }
           
        }

        public async Task UpdatePerson(Person person)
        {
            var personEntity = await _context.Persons.Include(a => a.Phones).FirstOrDefaultAsync(a => a.Id.Equals(person.Id));
            if (personEntity != null)
            {
                personEntity.FirstName = person.FirstName;
                personEntity.LastName = person.LastName;
                personEntity.Gender = person.Gender;
                personEntity.PersonalNumber = person.PersonalNumber;
                personEntity.BirthDate = person.BirthDate;
                personEntity.CityId = person.CityId;
                foreach (var phone in person.PhoneNumbers)
                {
                  var existingPhoneEntity = personEntity.Phones
                     .Where(a => a.Id == phone.Id)
                     .FirstOrDefault();
                    if(existingPhoneEntity != null)
                    {
                        existingPhoneEntity.PhoneNumber = phone.Number;
                        existingPhoneEntity.PhoneType =(Entities.Enums.PhoneTypeEnum)phone.Type;
                        _context.Entry(existingPhoneEntity).State = EntityState.Modified;
                    }
                }
                _context.Entry(personEntity).State = EntityState.Modified;
            }
            else
            {
                throw new Common.Exceptions.UserNotFoundException("User Not Found");
            }
        }

        public async Task<List<Person>> GetPersonsReport()
        {
            return await Task.Run(() =>
            {
                var groupedRelatedPersons = _context.RelatedPersons.AsEnumerable()
                        .GroupBy(a => new { a.PersonId, a.RelatedPersonType }).Select(b => new
                        { personId = b.Key.PersonId, relationTypeId = b.Key.RelatedPersonType, count = b.Count() });

                List<Person> personList = new List<Domain.Entities.Person>();

                foreach (var person in groupedRelatedPersons)
                {
                    var onePerson = Person.LoadReport(person.personId, person.count, (int)person.relationTypeId);
                    personList.Add(onePerson);
                }
                return personList;
            });
        }
    }
}

