﻿using Microsoft.EntityFrameworkCore;
using PersonGuide.Common.Exceptions;
using PersonGuide.Domain.Entities;
using PersonGuide.Domain.Interfaces.RelatedPerson;
using PersonGuide.Domain.Tools;
using PersonGuide.Persistence.Context;
using PersonGuide.Persistence.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonGuide.Persistence.Repositories
{
    public class RelatedPersonRepository : IRelatedPersonWriteOnlyRepository
    {
        private readonly PersonGuideContext _context;
        public IUnitOfWork UnitOfWork
        {
            get
            {
                return _context;
            }
        }
        public RelatedPersonRepository(PersonGuideContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));

        }

        public async Task CreateRelatedPerson(RelatedPerson person)
        {
            var personEntity = await _context.Persons.FirstOrDefaultAsync(a => a.Id.Equals(person.PersonId));
            if (personEntity == null)
            {
                throw new UserNotFoundException("Person Doesnot Exist for relaited person");
            }
            var relatedPersonEntity=new Entities.RelatedPerson
            {
                  PersonId=person.PersonId,
                  RelatedPersonType= (RelatedPersonTypeEnum)person.PersonType
            };
            await _context.RelatedPersons.AddAsync(relatedPersonEntity);
        }

        public async Task DeleteRelatedPerson(int Id)
        {
            var relatedPerson = await _context.RelatedPersons.FirstOrDefaultAsync(a => a.Id.Equals(Id));
            if (relatedPerson != null)
            {
                _context.Remove(relatedPerson);
            }
        }
    }
}
