﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Persistence.Entities
{
    public class Person
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Gender { get; set; }
        public string PersonalNumber { get; set; }
        public DateTime BirthDate { get; set; }
        public string Image { get; set; }
        public int CityId { get; set; }
        public City City { get; set; }
        public ICollection<Phone> Phones { get; set; }
        public ICollection<RelatedPerson> RelatedPersons { get; set; }
    }
}
