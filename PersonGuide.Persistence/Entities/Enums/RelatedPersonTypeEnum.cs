﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Persistence.Entities.Enums
{
    public enum RelatedPersonTypeEnum
    {
        Colleague,
        Aquainted,
        Relative,
        Other
    }
}
