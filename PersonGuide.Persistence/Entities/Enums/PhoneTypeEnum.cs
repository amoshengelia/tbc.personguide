﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Persistence.Entities.Enums
{
    public enum PhoneTypeEnum
    {
        Mobile,
        Office,
        Home
    }
}
