﻿using PersonGuide.Persistence.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Persistence.Entities
{
    public class RelatedPerson
    {
        public int Id { get; set; }
        public string PersonalNumber { get; set; }
        public RelatedPersonTypeEnum RelatedPersonType { get; set; }
        public Person Person { get; set; }
        public int PersonId { get; set; }
    }
}
