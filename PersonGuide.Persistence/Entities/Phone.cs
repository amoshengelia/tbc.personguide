﻿using PersonGuide.Persistence.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Persistence.Entities
{
    public class Phone
    {
        public int Id { get; set; }
        public string PhoneNumber { get; set; }
        public PhoneTypeEnum PhoneType { get; set; }
        public Person Person { get; set; }
        public int PersonId { get; set; }
    }
}
