﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using PersonGuide.Domain.Tools;
using PersonGuide.Persistence.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PersonGuide.Persistence.Context
{
    public class PersonGuideContext : DbContext, IUnitOfWork
    {
        public DbSet<Person> Persons { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Phone> Phones { get; set; }
        public DbSet<RelatedPerson> RelatedPersons { get; set; }

        public PersonGuideContext(DbContextOptions<PersonGuideContext> options) : base(options)
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.json")
            .Build();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("PersonGuide"));
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>(entity =>
            {

                entity.Property(a => a.Id).ValueGeneratedOnAdd();
                entity.Property(a => a.FirstName).HasMaxLength(2);
                entity.Property(a => a.FirstName).HasMaxLength(50);
                entity.Property(a => a.LastName).HasMaxLength(2);
                entity.Property(a => a.LastName).HasMaxLength(50);
                entity.Property(a => a.PersonalNumber).IsFixedLength(true);
                entity.Property(a => a.PersonalNumber).IsRequired();
                entity.Property(a => a.PersonalNumber).IsRequired();
                entity.HasOne(a => a.City)
                    .WithMany(b => b.Persons)
                    .HasForeignKey(d => d.CityId);
            });
            modelBuilder.Entity<Phone>(entity =>
            {
                entity.Property(a => a.PhoneNumber).HasMaxLength(4);
                entity.Property(a => a.PhoneNumber).HasMaxLength(50);
                entity.Property(a => a.PhoneType).HasConversion<int>();
                entity.HasOne(a => a.Person)
                    .WithMany(b => b.Phones)
                    .HasForeignKey(d => d.PersonId);
            });
            modelBuilder.Entity<RelatedPerson>(entity =>
            {
                entity.Property(a => a.RelatedPersonType).HasConversion<int>();
            });
            modelBuilder.Seed();
        }
        public async Task<bool> SaveEntitiesAsync()
        {
            try
            {
                return await SaveChangesAsync() > 0;
            }
            catch (DbUpdateConcurrencyException ex)
            {
                foreach (var entry in ex.Entries)
                {

                    entry.State = EntityState.Detached;

                }
                throw ex;
            }
        }
    }
    public class PersonGuideContextDesignFactory : IDesignTimeDbContextFactory<PersonGuideContext>
    {
        public PersonGuideContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
           .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
           .AddJsonFile("appsettings.json")
           .Build();
            var builder = new DbContextOptionsBuilder<PersonGuideContext>();
            var connectionString = configuration.GetConnectionString("PersonGuide");
            builder.UseSqlServer(connectionString); ;
            return new PersonGuideContext(builder.Options);
        }
    }
}
