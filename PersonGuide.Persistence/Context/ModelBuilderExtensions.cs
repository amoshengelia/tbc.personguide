﻿using Microsoft.EntityFrameworkCore;
using PersonGuide.Persistence.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersonGuide.Persistence.Context
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<City>().HasData(
                new City { Id=1, Name="Tbilisi"},
                new City {Id=2, Name="Kutaisi"},
                new City {Id=3, Name="Batumi"}
            );
           
        }
    }
}
